var fluentLogger = require('fluent-logger');
var format = require('util').format;

/**
 * Emit `access` event to fluent.
 */

module.exports = function() {
  return function(req, res, next) {
    var sock = req.socket;
    var remoteAddress = sock.socket ? sock.socket.remoteAddress : sock.remoteAddress;
    var method = req.method;
    var url = req.url;
    var httpVersion = req.httpVersion;
    var startTime = Date.now();

    function logRequest() {
      res.removeListener('finish', logRequest);
      res.removeListener('close', logRequest);

      var len = parseInt(res.getHeader('Content-Length'), 10);

      fluentLogger.emit('access', {
        'remote-address': remoteAddress,
        'method': method,
        'url': url,
        'http-version': httpVersion,
        'status': res.statusCode,
        'res[content-length]': isNaN(len) ? '' : len,
        'response-time': Date.now() - startTime
      });
    }

    res.on('finish', logRequest);
    res.on('close', logRequest);

    next();
  };
};

