var express = require('express');
var config = require('config');
var fluentLogger = require('fluent-logger').configure(require('./package.json').name, config.fluent);
var packageJson = require('./package.json');
var accessLogger = require('./lib/access-logger');
var errorHandler = require('./lib/error-handler');
var post = require('./lib/routes/post');
var app = express();

app.use(accessLogger());
app.use(app.router);
app.use(errorHandler());

app.get('/', post.index);
app.get('/posts', post.index);
app.post('/posts', post.create);
app.get('/posts/:post', post.show);

if (!module.parent) {
  app.listen(config.port);
  console.log('listening on port %d', config.port);
}
